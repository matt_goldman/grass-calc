import { Injectable } from '@angular/core';
import { Card } from './Models/card';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  constructor() { }

  cards: Card[] = [
    { id: 1, name: '$5,000', value: 5000, imageUrl: '' },
    { id: 2, name: '$25,000', value: 25000 },
    { id: 3, name: '$50,000', value: 50000 },
    { id: 4, name: 'Sold Out', value: -25000 },
    { id: 5, name: 'Doublecrossed', value: -50000 },
    { id: 6, name: 'Utterly Wiped Out', value: -100000 },
    { id: 7, name: 'Banker', value: 0 },
  ]
}
