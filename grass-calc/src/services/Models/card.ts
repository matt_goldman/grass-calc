export interface Card {
    id: number;
    name: string;
    value: number;
    imageUrl?: string;
}