import { Card } from './card';

export interface Hand {
    cards: Array<{card: Card, multiplier: number}>;
}