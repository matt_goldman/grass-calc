import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlayersService {

  constructor() { }

  players: string[];

  addPlayer(name: string){
    this.players.push(name);
  }

  getPlayers(): string[]{
    return this.players;
  }
}
