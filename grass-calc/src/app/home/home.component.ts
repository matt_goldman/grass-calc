import { Component, OnInit, RootRenderer } from '@angular/core';
import { PlayersService } from '../../services/players-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  constructor(private playersService: PlayersService) { }

  ngOnInit() {
  }

  addPlayer(name: string){
    this.playersService.addPlayer(name);
  }

}
